export const pieceofcake = {};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 pieceofcake.abilities = {
  "int": "pieceofcake.AbilityInt",
  "cha": "pieceofcake.AbilityCha",
  "ath": "pieceofcake.AbilityAth",
  "nrd": "pieceofcake.AbilityNrd"
};

pieceofcake.abilityAbbreviations = {
  "int": "pieceofcake.AbilityIntAbbr",
  "cha": "pieceofcake.AbilityChaAbbr",
  "ath": "pieceofcake.AbilityAthAbbr",
  "nrd": "pieceofcake.AbilityNrdAbbr"
};