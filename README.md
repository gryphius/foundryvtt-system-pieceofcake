# The "Piece of Cake" System for FoundryVTT

This is a foundryvtt system for the [Piece of Cake](https://gitlab.switch.ch/security-awareness/piece-of-cake) security awareness roleplaying game.
It is a very simple systen which basically only has 4 abilities for D20 rolls and not much else, there is no combat, no items, no effects etc.

